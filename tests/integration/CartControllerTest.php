<?php

class CartControllerTest extends LocalWebTestCase
{
	/**
	 * @after
	 */
	public function afterTest(){
		$this->client->get('/api/v1/cart/clear');
	}

	public function testEmpty(){
		$this->client->get('/api/v1/cart');
		$this->assertEquals(200, $this->client->response->getStatusCode());

		$dataAll = $this->client->response->getBody();
		$dataAll = json_decode($dataAll, true);

		$this->assertEmpty($dataAll);
	}

	public function testAddItems(){
		$expectedResult = [];

		for($i=1; $i<=20; $i++){
			$this->client->post('/api/v1/cart', ['name' => 'test' . $i, 'article_id' => $i + 1]);
			$this->assertEquals(200, $this->client->response->getStatusCode());

			$data = $this->client->response->getBody();
			$data = json_decode($data, true);

			$expected = ['id' => $i, 'name' => 'test' . $i, 'count' => 1, 'article_id' => $i + 1];
			$this->assertEquals($expected, $data);

			$expectedResult[] = $expected;
		}

		// get all items of the cart to make sure that the item really was added
		$this->client->get('/api/v1/cart');
		$dataAll = $this->client->response->getBody();
		$dataAll = json_decode($dataAll, true);

		$this->assertEquals($expectedResult, $dataAll);
	}

	public function testAddDuplicateItem(){
		$this->client->post('/api/v1/cart', ['name' => 'test', 'article_id' => 123]);
		$this->assertEquals(200, $this->client->response->getStatusCode());

		$this->client->post('/api/v1/cart', ['name' => 'test', 'article_id' => 123]);
		$this->assertEquals(200, $this->client->response->getStatusCode());

		$data = $this->client->response->getBody();
		$data = json_decode($data, true);

		$expected = ['id' => 1, 'name' => 'test', 'count' => 2, 'article_id' => 123];
		$this->assertEquals($expected, $data);
	}

	public function testRemoveItem(){
		$expectedResult = [];

		for($i=1; $i<=10; $i++){
			$this->client->post('/api/v1/cart', ['name' => 'test' . $i, 'article_id' => $i + 1]);
			$this->assertEquals(200, $this->client->response->getStatusCode());

			$data = $this->client->response->getBody();
			$data = json_decode($data, true);

			$expected = ['id' => $i, 'name' => 'test' . $i, 'count' => 1, 'article_id' => $i + 1];
			$this->assertEquals($expected, $data);

			$expectedResult[] = $expected;
		}

		// get all items of the cart to make sure that the item really was added
		$this->client->get('/api/v1/cart');
		$dataAll = $this->client->response->getBody();
		$dataAll = json_decode($dataAll, true);

		$this->assertEquals($expectedResult, $dataAll);

		// Test deletion of item with id 1
		$this->client->delete('/api/v1/cart/1');
		$this->assertEquals(200, $this->client->response->getStatusCode());

		// check that item 1 is not in the cart anymore
		$this->client->get('/api/v1/cart');
		$dataDeleteCart = $this->client->response->getBody();
		$dataDeleteCart = json_decode($dataDeleteCart, true);

		$lookup = array_filter($dataDeleteCart, function($item){
			return $item["id"] === 1;
		});

		// the removed item should not be in the cart anymore
		$this->assertEmpty($lookup);
		$this->assertEquals(count($expectedResult) - 1, count($dataDeleteCart));
	}

	public function testUpdateItem(){
		$this->client->post('/api/v1/cart', ['name' => 'test', 'article_id' => 123]);
		$this->assertEquals(200, $this->client->response->getStatusCode());

		$data = $this->client->response->getBody();
		$data = json_decode($data, true);

		// change the count for the item and update it
		$data['count'] = 500;

		$this->client->put('/api/v1/cart/' . $data['id'], $data);
		$this->assertEquals(200, $this->client->response->getStatusCode());

		$dataUpdate = $this->client->response->getBody();
		$dataUpdate = json_decode($dataUpdate, true);

		$this->assertEquals($dataUpdate['count'], 500);

		// assert that the item is really updated in the cart, and that the cart content is as expected
		$this->client->get('/api/v1/cart');
		$dataCart = $this->client->response->getBody();
		$dataCart = json_decode($dataCart, true);

		$this->assertEquals(1, count($dataCart));
		$this->assertEquals($dataCart[0]['count'], 500);
	}
}
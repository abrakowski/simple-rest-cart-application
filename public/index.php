<?php
session_start();

require_once '../vendor/autoload.php';
$composer = json_decode(file_get_contents(__DIR__ . '/../composer.json'));

$app = new \Slim\App([
	'version'        => $composer->version,
	'debug'          => false,
	'mode'           => 'production'
]);

require_once __DIR__ . '/../app/app.php';
$app->run();
<?php
namespace App\Controllers;

use App\Services\CartService;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class CartController, it contains all the methods neccessary to manage a cart from an REST endpoint
 *
 * @package App\Controllers
 */
class CartController
{
	protected $container;

	// constructor receives container instance
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	/**
	 * Helper function to extract the cart item from the request body
	 *
	 * @param $request ServerRequestInterface The request object as given by the router
	 * @return array - The extracted cart item
	 */
	public function getItemFromRequest($request){
		$parsedItem = CartService::prepareItem($request->getParsedBody());
		return $parsedItem;
	}

	/**
	 * Updates a given cart item
	 *
	 * @param $request ServerRequestInterface The request object as given by the router
	 * @param $response ResponseInterface The response object as given by the router
	 * @param $args array The url args as given by the router
	 *
	 * @return mixed The response containing the updated cart item
	 */
	public function update($request, $response, $args){
		$parseItem = $this->getItemFromRequest($request);
		$item = $this->container->cart->update(intval($args['id']), $parseItem);

		return $response->withJson($item, 200);
	}

	/**
	 * Creates a given cart item
	 *
	 * @param $request ServerRequestInterface The request object as given by the router
	 * @param $response ResponseInterface The response object as given by the router
	 * @param $args array The url args as given by the router
	 *
	 * @return mixed The response containing the created cart item
	 */
	public function create($request, $response, $args){
		$parseItem = $this->getItemFromRequest($request);
		$item = $this->container->cart->add($parseItem);

		return $response->withJson($item, 200);
	}

	/**
	 * Delete a cart item by its id
	 *
	 * @param $request ServerRequestInterface The request object as given by the router
	 * @param $response ResponseInterface The response object as given by the router
	 * @param $args array The url args as given by the router
	 *
	 * @return mixed The response containing the created cart item
	 */
	public function delete($request, $response, $args){
		$delete = $this->container->cart->remove(intval($args['id']));

		if($delete){
			return $response->withStatus(200);
		}

		return $response->withStatus(404);
	}

	/**
	 * Retrieves a cart item by its id
	 *
	 * @param $request ServerRequestInterface The request object as given by the router
	 * @param $response ResponseInterface The response object as given by the router
	 * @param $args array The url args as given by the router
	 *
	 * @return mixed The response containing retrieved cart item
	 */
	public function get($request, $response, $args){
		return $response->withJson($this->container->cart->getAll(), 200);
	}

	/**
	 * Retrieves a cart item by its id
	 *
	 * @param $request ServerRequestInterface The request object as given by the router
	 * @param $response ResponseInterface The response object as given by the router
	 * @param $args array The url args as given by the router
	 *
	 * @return ResponseInterface The response
	 */
	public function clear($request, $response, $args){
		$this->container->cart->clear();

		return $response->withStatus(200);
	}
}
<?php
namespace App\Services;

/**
 * Class CartService, this is responsible for managing our internal cart data.
 * We currently use the SESSION to store our cart, because a database is out of scope for this sample project.
 *
 * @package App\Services
 */
class CartService
{
	/**
	 * CartService constructor, which initially sets up the session variables
	 */
	public function __construct(){
		if(!isset($_SESSION['cart'])){
			$this->clear();
		}
	}

	/**
	 * Given a data array, this function tries to convert it into a valid cart item.
	 *
	 * @param $item array possible cart item data
	 * @return array a valid cart item
	 */
	public static function prepareItem($item){
		if(!is_array($item)){
			$item = [];
		}

		return array_merge([
			'id' => -1,
			'article_id' => -1,
			'count' => 1,
			'name' => ''
		], $item);
	}

	/**
	 * Retrieves all cart items from the cart
	 *
	 * @return mixed all cart items
	 */
	public function getAll(){
		return $_SESSION['cart'];
	}

	/**
	 * Tries to retrieve a cart item by the given id.
	 * If no cart item is found with the given id, "null" will be returned.
	 *
	 * @param $id int the cart item id
	 *
	 * @return null, if the cart item was not found, otherwise an array
	 */
	public function get($id){
		foreach($_SESSION['cart'] as $item){
			if($item['id'] === $id) return $item;
		}

		return null;
	}

	/**
	 * Tries to retrieve a cart item by the given article id.
	 * If no cart item is found with the given article id, "null" will be returned.
	 *
	 * @param $id int the article id
	 *
	 * @return null, if a cart item was not found, otherwise an array
	 */
	public function getByArticleId($id){
		foreach($_SESSION['cart'] as $item){
			if($item['article_id'] === $id) return $item;
		}

		return null;
	}

	/**
	 * Tries to get the position at which a given cart item is in the cart.
	 *
	 * @param $id int the cart item id
	 *
	 * @return int -1, if the cart item was not found
	 *             >= 0, otherwise
	 */
	public function getIndex($id){
		foreach($_SESSION['cart'] as $index => $item){
			if($item['id'] === $id) return $index;
		}

		return -1;
	}

	/**
	 * Adds a given cart item to the cart.
	 * If an item with the same article_id already exists, it will only increase the count property of the card item.
	 *
	 * @param $item array the cart item to add to the cart
	 * @return array the cart item
	 */
	public function add($item){
		$found = ($item['article_id'] > -1) ? $this->getByArticleId($item['article_id']) : null;

		if($found !== null){
			$found['count']++;

			return $found;
		} else {
			$item['id'] = ++$_SESSION['id'];

			$_SESSION['cart'][] = $item;
			return $item;
		}
	}

	/**
	 * Updates a cart item with the given cart item id.
	 * It will add the given cart item to the end of the list, if it is still not in the list.
	 * If a cart item was found with the given id, it will be updated with the given data.
	 *
	 * @param $id int the id of the cart that should be updated
	 * @param $item array the update data for the cart item
	 *
	 * @return array the updated cart item
	 */
	public function update($id, $item){
		$foundIndex = $this->getIndex($id);
		$item['id'] = $id;

		if($foundIndex === -1){
			$_SESSION['cart'][] = $item;
		} else {
			$_SESSION['cart'][$foundIndex] = $item;
		}

		return $item;
	}

	/**
	 * Given a cart item id, it will try to remove the cart item with the same id from the cart.
	 *
	 * @param $id int the id of the cart time, that should be removed
	 *
	 * @return bool true, if an cart item could be removed
	 *              false, otherwise
	 */
	public function remove($id){
		$foundIndex = $this->getIndex($id);

		if($foundIndex > -1){
			unset($_SESSION['cart'][$foundIndex]);
			return true;
		}

		return false;
	}


	/**
	 * Removes all cart items from the cart
	 */
	public function clear(){
		$_SESSION['cart'] = [];
		$_SESSION['id'] = 0;
	}
}
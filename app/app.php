<?php
namespace App;

/**
 * @var $app \Slim\App
 */

$container = $app->getContainer();
$container['cart'] = function($container){
	$service = new Services\CartService();
	return $service;
};

$app->group('/api/v1/cart', function(){
	$this->get('/clear', 'App\Controllers\CartController:clear');
	$this->get('', 'App\Controllers\CartController:get');
	$this->post('', 'App\Controllers\CartController:create');

	$this->group('/{id:[0-9]+}', function(){
		$this->delete('', 'App\Controllers\CartController:delete');
		$this->put('', 'App\Controllers\CartController:update');
	});
});
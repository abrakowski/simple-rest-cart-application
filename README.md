# Simple Cart REST Application

This project uses the [Slim framework](https://www.slimframework.com) for the application API endpoints. At its core, Slim is a dispatcher that receives an HTTP request, invokes an appropriate callback routine, and returns an HTTP response. That’s it.

## Requirements
* Composer
* PHP 5.5 or newer

## Usage
1. Clone repository, and open a terminal in the cloned project folder
2. Run `composer install`
3. Run `composer dump-autoload`
4. To run the tests, execute `composer test`
 
Done!